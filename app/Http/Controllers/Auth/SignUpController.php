<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class SignUpController extends Controller
{
    public function register(Request $request)
    {
        $validate = $request->validate([
            'username' => 'required|string|max:255|unique:users',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string',
            'phone' => 'required|string',
            'address' => 'required|string',
            'city' => 'required|string',
            'country' => 'required|string',
            'name' => 'required|string',
            'postcode' => 'required|string',
            ]);

            $user = User::create([
                'username' => $validate['username'],
                'email' => $validate['email'],
                'password' => Hash::make($validate['password']),
                'phone' => $validate['phone'],
                'address' => $validate['address'],
                'city' => $validate['city'],
                'country' => $validate['country'],
                'name' => $validate['name'],
                'postcode' => $validate['postcode'],
            ]);

            $token = $user->createToken('auth_token')->plainTextToken;

            return response()->json([
                    'data' => $user,
                    'access_token' => $token,
                    'token_type' => 'Bearer',
            ]);
    }
}

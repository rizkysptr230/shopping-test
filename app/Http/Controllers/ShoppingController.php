<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Shopping;

class ShoppingController extends Controller
{
    public function index()
    {
        $shopping = Shopping::all();

        return response()->json($shopping);
    }

    public function store(Request $request)
    {
        $shopping = Shopping::create([
            'name' => $request->name,
            'createdDate' => $request->createdDate,
        ]);
        return response()->json($shopping);
    }

    public function getId($id)
    {
        $shopping = Shopping::where('id', $id)->first();
        return response()->json($shopping);
    }

    public function update(Request $request)
    {
        $shopping = Shopping::find($request->id);
        $data = [
            'name' => $request->name,
            'createdDate' => $request->createdDate,
        ];
        $shopping->update($data);

        return response()->json($shopping);

    }

    public function destroy($id)
    {
        Shopping::find($id)->delete();
        return response()->json(['success' => 'Shopping Deleted Successfull']);
    }

}

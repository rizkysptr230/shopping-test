<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\SignUpController;
use App\Http\Controllers\Auth\SignInController;
use App\Http\Controllers\ShoppingController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/users/signup', [SignUpController::class, 'register']);
Route::post('/users/signin', [SignInController::class, 'login']);
Route::post('/users/logout', [SignInController::class, 'logout'])->middleware('auth:sanctum');
Route::post('/users', [SignInController::class, 'user'])->middleware('auth:sanctum');
Route::get('/shopping', [ShoppingController::class, 'index'])->middleware('auth:sanctum');
Route::post('/shopping', [ShoppingController::class, 'store'])->middleware('auth:sanctum');
Route::get('/shopping/{id}', [ShoppingController::class, 'getId'])->middleware('auth:sanctum');
Route::put('/shopping/{id}', [ShoppingController::class, 'update'])->middleware('auth:sanctum');
Route::delete('/shopping/{id}', [ShoppingController::class, 'destroy'])->middleware('auth:sanctum');
